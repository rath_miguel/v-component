## セットアップ

### npmファイルのインストール

```
npm install
```

### Bowerファイルのインストール
初回起動時はインストール前に必要になりそうなファイルを把握してbower.jsonを書き換えておくとよい

```
bower install
```

### ビルドの実行
初回起動時にconfig.yml内を確認

```
gulp build
```

## テストサーバーのセットアップ

### vagrantのセットアップ
/www/public/ディレクトリを作成して下記コマンドを実行

```
vagrant up
```

### wordpressのセットアップ
vagrantにsshログインした後、下記手順でコマンド実行。
複数のディレクトリに設置する場合は2.から適宜移動ディレクトリを変えて実行

```
# 1.wp Cliのアップデート
sudo wp cli update --allow-root

# 2.インストールディレクトリへ移動
cd /var/www/public/blog

# 3.Wordpress本体のダウンロード
wp core download --locale=ja

# 4.wp-configの生成
wp core config --dbname=scotchbox --dbuser=root --dbpass=root --dbhost=localhost --dbprefix=wp_

# 5.Wordpressのインストール
wp core install --url=http://test.dev/blog/ --title=blog --admin_user=vzone --admin_password=rekov999 --admin_email=info@vzone.co.jp

# プラグインのインストール
wp plugin install advanced-custom-fields all-in-one-wp-migration duplicate-post post-thumbnail-editor regenerate-thumbnails taxonomy-terms-order wordpress-importer --activate
```