###
ユーザーエージェント判別
_ua.Tablet -> タブレットの場合のみtrue
_ua.Mobile -> スマホの場合のみtrue
###

window._ua = ((u) ->
	Tablet: u.indexOf('windows') != -1 and u.indexOf('touch') != -1 and u.indexOf('tablet pc') == -1 or u.indexOf('ipad') != -1 or u.indexOf('android') != -1 and u.indexOf('mobile') == -1 or u.indexOf('firefox') != -1 and u.indexOf('tablet') != -1 or u.indexOf('kindle') != -1 or u.indexOf('silk') != -1 or u.indexOf('playbook') != -1
	Mobile: u.indexOf('windows') != -1 and u.indexOf('phone') != -1 or u.indexOf('iphone') != -1 or u.indexOf('ipod') != -1 or u.indexOf('android') != -1 and u.indexOf('mobile') != -1 or u.indexOf('firefox') != -1 and u.indexOf('mobile') != -1 or u.indexOf('blackberry') != -1
)(window.navigator.userAgent.toLowerCase())