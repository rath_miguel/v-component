###
タブレットの時だけビューポート変更の操作
viewport('画面幅')
###
viewport('1240')

###
スマホの時だけ電話リンクを付与
tel('対象のクラス', '電話番号')
###
tel('.tel', '0857000000')

###
トップへ戻るボタンの生成
html: 生成するHTML
target: スムーススクロールや出現条件の対象になるidまたはclass
###
totop
	html: '<div id="totop"><a href="#"><i class="fa fa-angle-up"></i></a></div>'
	target: '#totop'

$ ->
	# グリッドの高さを揃える
	$('.grids').matchHeight();

	# イメージのポップアップ
	$('.js-modal').magnificPopup
		type: 'inline'
		zoom:
			enabled: true
			duration: 120
			easing: 'ease-in-out'
			showCloseBtn: false

	# 郵便番号検索ボタン
	$('#zip-number').jpostal
		click: '#zip-button'
		postcode: '#zip-number'
		address:
			'#zip-pref': '%3'
			'#zip-address': '%4%5'

# スライダー
# $(window).load ->
# 	swiper = new Swiper('.swiper-container',
# 		loop: true
# 		# nextButton: '.swiper-button-next'
# 		# prevButton: '.swiper-button-prev'
# 		pagination: '.swiper-pagination'
# 		paginationClickable: true
# 		autoplay: 3000
# 		speed: 600)