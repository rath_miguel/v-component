'use strict';

const gulp = require('gulp');
const config = require('../config').config;

const ext = require('gulp-ext-replace');

// change html => dwt
gulp.task('change:tmpl' , () => {
  return gulp.src('./web/Templates/*.html')
  .pipe(ext('.dwt'))
  .pipe(gulp.dest('./public/Templates/'));
});

// change html => txt
gulp.task('change:txt' , () => {
  return gulp.src(['./web/cgi-bin/**/mail.html', './web/cgi-bin/**/reply.html'],{base: 'web/'})
  .pipe(ext('.txt'))
  .pipe(gulp.dest('./public/'));
});

// change html => php
gulp.task('change:php' , () => {
  return gulp.src(['./web/**/*.html', '!./web/cgi-bin/**/*', '!./web/Templates/**/*'])
  .pipe(ext('.php'))
  .pipe(gulp.dest('./public/'));
});

gulp.task('change', ['change:tmpl', 'change:txt', 'change:php']);