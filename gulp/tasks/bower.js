'use strict';

const gulp = require('gulp');
const config = require('../config').config;

const inject = require('gulp-inject');
const bower = require('main-bower-files');
const filter = require('gulp-filter');

const concat = require('gulp-concat');
const header = require('gulp-header');

const del = require('del');

// concat JS Files
gulp.task('bower', function() {
  let jsFilter = filter(['**/*.js']);
  let cssFilter = filter(['**/*.css']);

  console.log('▼以下のファイルを抽出しました。');
  console.log(bower());

  gulp.src(bower())
  .pipe(jsFilter)
  .pipe(concat('plugin.js'))
  .pipe(header(config.plugins))
  .pipe(gulp.dest(`${config.path.output + config.path.js}`))

  gulp.src(bower())
  .pipe(cssFilter)
  .pipe(concat('plugin.css'))
  .pipe(header(config.plugins))
  .pipe(gulp.dest(`${config.path.output + config.path.css}`))
});