'use strict';

const gulp = require('gulp');
const config = require('../config').config;

const jade = require('gulp-jade');
require('jade').filters.code = function(block) {
  return block.replace(/&/g, '&amp;')
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;')
  .replace(/"/g, '&quot;')
  .replace(/'/g, '&#039;');
};

const data = require('gulp-data');

const plumber  = require('gulp-plumber');
const notify = require("gulp-notify");
const cache = require('gulp-cached');

gulp.task('jade', () => {
  return gulp.src([`${config.path.input}jade/**/*.jade`,`!${config.path.input}jade/**/_*.jade`])
    // .pipe(cache('jaded'))
    .pipe(plumber({errorHandler: notify.onError('<%= error.message %>')}))
    .pipe(data((file) => {
      return config;
    }))
    .pipe(jade({
      pretty: true,
      basedir: `${config.path.input}jade/`
    }))
    .pipe(gulp.dest(config.path.output));
});