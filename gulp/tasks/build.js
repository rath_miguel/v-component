'use strict';

const gulp = require('gulp');
const config = require('../config').config;

gulp.task('build', ['bower', 'jade', 'sass', 'coffee','imagemin']);