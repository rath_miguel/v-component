'use strict';

const gulp = require('gulp');
const config = require('../config').config;

const cache = require('gulp-cached');

gulp.task('copy:clipmail', () => {
  for (var value in config.clipmail) {
    gulp.src('tools/clipmail/**/*')
    .pipe(gulp.dest(`./public/cgi-bin/${config.clipmail[value]}-clipmail/`));
    gulp.src([
      `./web/cgi-bin/${config.clipmail[value]}-clipmail/tmpl/conf.html`,
      `./web/cgi-bin/${config.clipmail[value]}-clipmail/tmpl/error.html`,
      `./web/cgi-bin/${config.clipmail[value]}-clipmail/tmpl/thanks.html`
      ])
    .pipe(gulp.dest(`./public/cgi-bin/${config.clipmail[value]}-clipmail/tmpl/`));
  }
});

gulp.task('copy:util', () => {
  gulp.src('tools/include/**/*')
  .pipe(gulp.dest('./public/include/'));
});

gulp.task('copy:image', () => {
  gulp.src('./web/**/*.+(jpg|jpeg|png|gif|svg)')
  .pipe(gulp.dest('./public/'));
});

gulp.task('copy:initfiles', () => {
  gulp.src('./src/jade/cgi-bin/**/init.cgi', {base: 'src/jade/'})
  .pipe(gulp.dest('./public/'));
});

gulp.task('copy:fonts', () => {
  gulp.src('./web/fonts/**/*')
  .pipe(gulp.dest('./public/fonts/'));
});

gulp.task('copy:misc', () => {
  gulp.src('./web/**/*.+(txt|js)')
  .pipe(gulp.dest('./public/'));
});

gulp.task('copy:stage', () => {
  gulp.src(['./public/**/*', '!./public/**/*.LCK', '!./public/png/**/*', '!./public/Templates/**/*'])
  .pipe(cache('copyStage'))
  .pipe(gulp.dest(config.path.www));
});

gulp.task('pullup', () => {

  gulp.src(`${config.path.release}**/*.+(jpg|jpeg|png|gif|svg|js|css)`)
  .pipe(gulp.dest(config.path.output));
});

gulp.task('copy', ['copy:clipmail', 'copy:util', 'copy:image', 'copy:initfiles', 'copy:misc', 'copy:fonts']);