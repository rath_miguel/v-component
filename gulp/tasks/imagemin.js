'use strict';

const gulp = require('gulp');
const config = require('../config').config;

const imagemin = require('gulp-imagemin');

gulp.task('imagemin', () => {
  gulp.src([`${config.path.release}**/*.+(jpg|jpeg|png|gif|svg)`, `!${config.path.release}png/**/*`])
  .pipe(imagemin({optimizationLevel: 7, }))
  .pipe(gulp.dest(config.path.output));
});